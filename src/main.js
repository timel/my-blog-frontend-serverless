import { createApp } from "vue";
import App from "./App.vue";
import ElementPlus from "element-plus";
import "element-plus/dist/index.css";
import "./assets/css/global.css";
import router from "./router";
import axios from "axios";
import VueAxios from "vue-axios";
import store from "./store";

const http = axios.create({
    baseURL: 'https://mongodb-web-test.vercel.app'
})

const app = createApp(App);
app.use(VueAxios, http);
app.use(store);
app.use(ElementPlus);
app.use(router);
app.mount("#app");
