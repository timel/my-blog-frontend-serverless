import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
	{
		path: "/",
		name: "index",
		component: () => import("../views/Index.vue"),
	},
	{
		path: "/:id",
		name: "article",
		component: () => import("../views/Article.vue"),
	},
	{
		path: "/:id/edit",
		name: "edit",
		component: () => import("../views/Edit.vue"),
	},
	{
		path: "/new",
		name: "new",
		component: () => import("../views/New.vue"),
	},
	{
		// 这样子捕获404
		path: "/:catchAll(.*)",
		component: () => import("../views/404.vue"),
	},
];

const router = createRouter({
	history: createWebHashHistory(),
	routes,
});

// 守护路由 无用
router.beforeEach((to, from) => {
	// ...
	// 返回 false 以取消导航
	// console.log(to, from);
	return true;
});

export default router;
