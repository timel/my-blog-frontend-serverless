// 读取文字，按照\n分割，返回样例如下
// {
// ['p', '<p>xxx<p/>'],
// ['img', '<img src="xxx"/>'],
// }

export const textToDict = (text) => {
	let article_dict = {};
	article_dict = text.split("\n");
	// 剪去尾部的\n
	while (article_dict[article_dict.length - 1] === "") {
		article_dict.pop();
	}
	// 变成innerhtml
	article_dict = article_dict.map((item) => {
		// 如果就是图片就直接返回
		if (RegExp(`^https?://.*\.(?:jpg|png|jpeg|webp|svg).*`).test(item)) {
			return ["img", item.trim()];
		}
		// 下面匹配前缀 比如h2: h3: img: 这样子
		let e = new RegExp("^.{1,8}?:");
		let prefix = e.exec(item);
		if (prefix) prefix = prefix[0].slice(0, -1);
		switch (true) {
			case /img/.test(prefix): // 匹配到图片
				return ["img", item.slice(4).trim()];
			case /:/.test(prefix): // 约定 ::小标题
				return ["h3", item.slice(2)];
			case /h[2-6]/.test(prefix): // 匹配小标题
				return [prefix, item.slice(3)];
			case /q/.test(prefix): // 引用
				return ["blockquote", item.slice(2)];
			case /c/.test(prefix): // caption 图注
				return ["mark", item.slice(2)];
			default:
				// 默认是按照段来匹配的
				return ["p", item];
		}
	});
	return article_dict;
};
