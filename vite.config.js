import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [vue()],
	base: './',
	// server: {
	// 	proxy: {
	// 		"/api": {
	// 			// target: "http://127.0.0.1:3000/articles",
	// 			target: "https://mongodb-web-test.vercel.app",
	// 			changeOrigin: true,
	// 			// rewrite: (path) => path.replace(/^\/api/, ""),
	// 			secure: false,
	// 			// ws: true,
	// 		},
	// 	},
	// },
});
